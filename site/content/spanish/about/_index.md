---
title: "Sobre Nosotros"
draft: false
# page title background image
bg_image: "images/backgrounds/data.jpg"
# about image
image: "images/about/logo.png"
# meta description
description : ""
---

## SOBRE NOSOTROS

ViaPostres es una iniciativa que apunta a proveer servicios de consultoría, gestión y desarrollo de Postgres y otros motores de Base de Datos, principalmente en el mercado Argentino. Conscientes de la características, las derivadas necesidades y la actual situación socio-económica; 

Nos proponemos a traer servicios y tecnologías basados en el conocimiento de nivel internacional a las empresas nacionales. Somos partners locales de __OnGres Inc.__, empresa de creciente reputación global.

Establecidos en el mercado por más de 10 años, sumando miembros en el equipo y comenzando una nueva etapa en la empresa, estableciendo nuevos asociados estratégicos. Todos nuestros profesionales han trabajado en Compañías de renombre internacional, manipulando y desarrollando infraestructuras de alta complejidad.

__¡Estamos Orgullosos de lo que hemos logrado hasta ahora y esperamos verlo en nuestra cartera de clientes!__
