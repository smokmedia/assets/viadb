---
title: "Administradores"
date: 2019-06-06T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/data.jpg"
# meta description
description : "Los cursos -Postgres DB Administrator- proveen los conocimientos necesarios para evaluar, mantener y perfeccionar el funcionamiento de un servidor PostgreSQL como una unidad funcional."
# course thumbnail
image: "images/courses/adm1.jpg"
# taxonomy
category: "Base de Datos"
# staff
staff: "Emanuel Calvo"
# duration
duration : " Month"
# weekly
weekly : " hours"
# course fee
fee : "From: $"
# apply url
apply_url : "#"
# type
type: "course"
---


### Alcance

Los egresados de los cursos -Postgres DB Administrator- obtendrán sólidos conocimientos de:

* Instalación / Confguración del servidor
* Pruebas de rendimiento (benchmarking)
* Elección / evaluación de hardware
* Diseño de Bases de datos 
* Indices
* Foreign Keys / Integridad referencial
* Monitoreo / Alertas



### Temario

Modulo I : Instalación / COnfiguración inicial

* Introducción
* Hardware

    * CPUs
    * Memoria
    * Dispositivos de almacenamiento. FIlesystems en Linux. Particiones. RAID
    * Herramientas de testeo / benchmarking del hardware

* Instalación

    * Versiones
    * Obtener las fuentes
    * Compilación
    * Upgrades

Módulo II : Aspectos del Diseño

* Introducción
* El modelo relacional. Surgimiento. Virtudes y defectos
* Tablas. Sus campos y tipos de datos. Indices, clave primaria
* Integridad referencial (Foreign Keys)
* Vistas: Ocultando complejidades de implementación

Módulo III : Monitoreo / Mantenimiento

* Introducción. Análisis. Estadísticas. Alertas
* Evaluando el estado del Servidor

    * Herramientas del S.O
    * Herramientas PostgreSQL / Tablas y vistas del Sistema
    * Herramientas externas. RRD - Nagios - Collectd
    * Análisis de logs - Collectd - pgfouine

* Capturando todas las consultas: pg_stat_statements
* Facilitando la lectura de logs: log_line_prefix
* Backups

Módulo IV : Rendimiento

* Uso efectivo de índices

    * Indices parciales
    * Indices insuficientes
    * Demasiados Indices!
    * Index Bloat
    * Clustering
    * Evaluando la eficacia de los índices. Las tablas pg_stat_*

* Usando Explain para conocer el plan de ejecución
* "Logueando" consultas lentas
* Ajustes de configuración para un mejor desempeño

    * shared_buffers
    * work_mem
    * mem_connections
    * checkpoint_segments
    
* pg_xlog / WAL
* VACCUM
* Checkpoints
* Bulk Loading

Módulo V : Seguridad

* El archivo pg_hba.conf
* Privilegios SQL
* La cláusula Security Invoker/Definer
* Conexiones via SSL


