---
title: "Desarrolladores SQL / PLSQL"
date: 2019-07-06T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/data.jpg"
# meta description
description : "Los cursos -PostgreSQL Developer- están orientados a programadores y desarrolladores de sistemas en general"
# course thumbnail
image: "images/courses/dev.jpg"
# taxonomy
category: "Base de Datos"
# staff
staff: "Emanuel Calvo"
# duration
duration : " Month"
# weekly
weekly : " hours"
# course fee
fee : "From: $"
# apply url
apply_url : "#"
# type
type: "course"
---


### Alcance

Los egresados de los cursos -PostgreSQL Developers- poseerán un fuerte conocimiento de:

* Lenguaje SQL
* Stored Procedures
* PL/SQL
* Triggers
* Views
* Transacciones

Lo cual los pone en una excelente posición para desarrollar complejas y potentes funcionalidades en el motor SQL de PostgreSQL

### Temario

Módulo I : Lenguaje SQL - Parte I

* Introducción
* Tipos de datos habituales
* Sentencias Básicas

    * INSERT
    * DELETE
    * UPDATE
    * SELECT

* Claúsulas adicionales

    * WHERE
    * IN
    * ANY/ALL
    * NOT IN
    * LIKE
    * ~
    * ORDER BY
    * LIMIT
    * OFFSET

Módulo I : Lenguaje SQL - Parte II

* JOINS

    * INNER JOIN
    * LEFT JOIN
    * RIGHT OUTER JOIN
    * CROSS JOIN

* SUB-SELECTs / Consultas anidadas
* Aggregates / Clausula GROUP BY

Módulo II : Views / Stored Procedures / Funciones

* Introducción
* Views / Vistas
* Stored Procedures / Funciones

    * Parámetros / Argumentos
    * Parámetros IN, OUT, INOUT
    * Valores por omisión

* Valores de retorno
* Sobrecarga de funciones
* Opciones de seguridad: SECURITY DEFINER/INVOKER
* Opciones de durabilidad: VOLATILE/STABLE/INMMUTABLE

Módulo III : PL/SQL

* PL/SQL

    * Introducción
    * Estructuras "procedurales"
    * Condicionales: IF / ELSE / ELSIF
    * Cíclicas: FOR / WHILE / EXIT / CONTINUE
    * Captura de errores: BEGIN / EXCEPTION
    * SELECT INTO
    * PERFORM
    * RETURN NEXT
    * RETURN QUERY

Módulo IV : Características avanzadas

* Transacciones
* La especificación ACID
* Triggers

    * Introducción
    * Tipos de Triggers: INSERT, DELETE, UPDATE
    * Declaración de los triggers
    * OLD y NEW
    * Argumentos en las funciones tipo trigger
    * BEGIN .. END / ROLLBACK

Módulo V : SQL Avanzado

* INSERT .. RETURNING
* DELETE .. RETURNING
* Tablas temporales / construcción WITH / CTE
* WINDOW FUNCTIONS
* Filtros de avanzada con Expresiones Regulares




### How to Apply

* Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae obcaecati unde nulla?
* Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae obcaecati unde nulla?
* Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae obcaecati unde nulla?
* Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae obcaecati unde nulla?
* Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae obcaecati unde nulla?


### Fees and Funding

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit
anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia
consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem
ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut
labore et dolore magnam aliquam quaerat voluptatem.