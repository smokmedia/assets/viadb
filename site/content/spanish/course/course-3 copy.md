---
title: "Arquitectos"
date: 2019-07-06T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/data.jpg"
# meta description
description : "Los cursos -PostgreSQL Architect- proveen los fundamentos y conocimientos necesarios para extender la funcionalidad de un servidor postgres, bien sea a través de -middleware-, o escalando la capacidad utilizando múltiples servidores en simultaneo"
# course thumbnail
image: "images/courses/arch1.jpg"
# taxonomy
category: "Base de Datos"
# staff
staff: "Emanuel Calvo"
# duration
duration : " Month"
# weekly
weekly : " hours"
# course fee
fee : "From: $"
# apply url
apply_url : "#"
# type
type: "course"
---


### Temario

Módulo I : Caching / Pooling

* Introducción
* Mamcached
* Pgmemcached
* PgBouncer
* PgPool

Módulo II : Replicación

* Introducción
* Replicación asincrónica
* log shipping 
* streaming replication
* Slony
* PgPool II
* Replicación Asincrónica
* PITR

Módulo III : Escabilidad

* Replicación & Balanceo de carga
* Data Partitioning
* PI / Proxy
* Postgres-XC
