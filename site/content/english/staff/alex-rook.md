---
title: "John Doe"
draft: false
# page title background image
bg_image: "images/backgrounds/data.jpg"
# meta description
description : ""
# staff portrait
image: "images/staff/staff-2.jpg"
# course
course: ""
# biography
bio: ""
# interest
interest: ["Computer Networking","Computer Security","Human Computer Interfacing"]
# contact info
contact:
  # contact item loop
  - name : "clarkmalik@email.com"
    icon : "ti-email" # icon pack : https://themify.me/themify-icons
    link : "mailto:clarkmalik@email.com"

  # contact item loop
  - name : "+12 034 5876"
    icon : "ti-mobile" # icon pack : https://themify.me/themify-icons
    link : "tel:+120345876"

  # contact item loop
  - name : "Clark Malik"
    icon : "ti-facebook" # icon pack : https://themify.me/themify-icons
    link : "#"

  # contact item loop
  - name : "Clark Malik"
    icon : "ti-twitter-alt" # icon pack : https://themify.me/themify-icons
    link : "#"

  # contact item loop
  - name : "Clark Malik"
    icon : "ti-skype" # icon pack : https://themify.me/themify-icons
    link : "#"

  # contact item loop
  - name : "clarkmalik.com"
    icon : "ti-world" # icon pack : https://themify.me/themify-icons
    link : "#"

  # contact item loop
  - name : "1313 Boulevard Cremazie,Quebec"
    icon : "ti-location-pin" # icon pack : https://themify.me/themify-icons
    link : "#"

# type
type: "staff"
---

### About Me

