---
title: "Coming Soon!"
# Schedule page publish date
publishDate: ""
# event date
date: ""
# post save as draft
draft: false
# page title background image
bg_image: "images/backgrounds/data.jpg"
# meta description
description : ""
# Event image
image: "images/events/comingsoon.jpg"
# location
location: "Buenos Aires, Argentina"
# entry fee
fee: "From: "
# apply url
apply_url : "#"
# event speaker
speaker:
  # speaker loop
  - name : ""
    image : "images/event-speakers/speaker-1.jpg"
    designation : ""

  # speaker loop
  - name : ""
    image : "images/event-speakers/speaker-2.jpg"
    designation : ""

  # speaker loop
  - name : ""
    image : "images/event-speakers/speaker-3.jpg"
    designation : ""

  # speaker loop
  - name : ""
    image : "images/event-speakers/speaker-4.jpg"
    designation : ""

# type
type: "event"
---

### About Event

-