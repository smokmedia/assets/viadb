---
title: "About Us"
draft: false
# page title background image
bg_image: "images/backgrounds/data.jpg"
# about image
image: "images/about/logo.png"
# meta description
description : "Description"

---


### ABOUT US

ViaPostgres is an initiative that aims to provide consulting, management and development services for Postgres and other Database engines, mainly in the Argentine Market. Aware of the characteristics

the derived needs and the current socio-economic situation; We propose to bring services and technologies based on international knowledge to national companies. We are local partners of __OnGres Inc.__, a company with a growing global reputation.
             
Established in the market for more than 10 years, adding members to the team and starting a new stage in the company, establishing new strategic partners. All of our professionals have worked in internationally renowned companies, manipulated and developed highly complex infrastructures.
             
__We are proud of what we have accomplished so far and look forward to seeing it in our client portfolio!__
