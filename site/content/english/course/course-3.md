---
title: "Administration"
date: 2019-07-06T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/data.jpg"
# meta description
description : "The Postgres DB administrator courses provide the necessary knowledge to evaluate, maintain and perfect the operation of a PostgreSQL server as a functional unit."
# course thumbnail
image: "images/courses/adm1.jpg"
# taxonomy
category: "Databases"
# staff
staff: "Emanuel Calvo"
# duration
duration : " Month"
# weekly
weekly : " hours"
# course fee
fee : "From: $"
# apply url
apply_url : "#"
# type
type: "course"
---


### About Course

Graduates of the Postgres DB administrator courses will obtain solid knowledge of: 

 * Server Installation / Configuration 
 * Performance test (benchmarking)
 * Hardware choice / Evaluation
 * Database desing 
 * Indices
 * Foreign Keys / Referential Integrity
 * Monitoring / Alerts


### Temary

First Module: Initial Installation / Configuration

* Introduction
* Hardware 

    * CPUs
    * Memory
    * Storage Devices / Filesystems on Linux / Partitions / RAID
    * Hardware testing / benchmarking tools

* Installation

    * Versions
    * Get the Sources
    * Compilation
    * Upgrades

Second Module: Desing Aspects

* Introduction
* The relational model / Emergence / Strengths and Weakness
* Boards / Fields and Data types / Indices and Primary Key
* Referential Integrity (Foreign Keys)
* Views: hiding implementation complexities

Third Module : Monitoring / Maintenance

* Introduction / Analysis / Statistics / Alerts
* Evaluating the status of the server

    * Tools of the S.O
    * PostgreSQL Tools / System tables and Views
    * External Tools. RRD - Nagios - Collectd
    * Logs Analysis - Collectd - pgfouine

* Capturing all queries: pg_stat_statements
* Making logs easier to read: log_line_prefix
* Backups

Fourth Module: Performance

* Effective use of indices

    * Partial Indices
    * Insufficient Indices
    * Too many indixes!
    * Index Bloat
    * Clustering
    * Evaluating the effectiveness of the indices / The pg_stat_* tables

* Using Explain to know the execution plan
* "Logging in" Slow queries
* Configuration settings for better performance

    * shared_buffers
    * work_mem
    * max_connections
    * checkpoint_segments

* pg_xlog / WAL
* Vacuum
* Checkpoints
* Bulk Loading

Fifth Module: Security

* The pg_hba.conf file
* SQL privileges
* The Security Invoker / Definer Clause
* Connections via SSL