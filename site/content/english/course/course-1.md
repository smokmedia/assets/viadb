---
title: "Architects"
date: 2019-07-06T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/data.jpg"
# meta description
description : "The PostgreSQL Architect courses provide the fundamentals and knowledge necessary to extend the functionality of a postgres server, either through middleware or by scalinbg the capacity using multiple servers simultaneously"
# course thumbnail
image: "images/courses/arch1.jpg"
# taxonomy
category: "Databases"
# staff
staff: "Emanuel Calvo"
# duration
duration : " Month"
# weekly
weekly : " hours"
# course fee
fee : "From: $"
# apply url
apply_url : "#"
# type
type: "course"
---

 
### Temary

First Module : Caching / Pooling

* Introduction
* memcached
* pgmemcached
* PgBouncer
* PgPool

Second Module: Replication

* Introduction
* Asynchronous Replication
* Log Shipping
* Streaming Replication
* Slony
* PgPool II
* Synchronous Replication
* PITR

Third Module: Scalability

* Replication and Load Balancing 
* Data Partitioning 
* PI / Proxy
* Postgres-XC